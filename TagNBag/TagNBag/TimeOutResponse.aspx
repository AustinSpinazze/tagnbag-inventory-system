﻿<%@ Page Title="Timeout Error" Language="C#" AutoEventWireup="true" CodeBehind="TimeOutResponse.aspx.cs" Inherits="TagNBag.TimeOutResponse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title><%: Page.Title %> - Tag 'N Bag</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>The connection to the server timed out. </p>
            <asp:Button ID="Button1" runat="server" Text="Return to Login" OnClick="Button1_Click" />
        </div>
    </form>
</body>
</html>
