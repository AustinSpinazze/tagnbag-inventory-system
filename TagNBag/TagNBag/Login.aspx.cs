﻿using System;
using System.Security;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace TagNBag
{
    public partial class Login : System.Web.UI.Page
    {
        private char[] pass = { 'T', 'e', 'n', 'n', 'i', 's', '0', '1' };
        protected void Page_Load(object sender, EventArgs e)
        {
            lblErrorMessage.Visible = false;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            using (SqlConnection sqlCon = new SqlConnection(@"Data Source=98.191.23.140, 1434;Initial Catalog=BagNTag; User Id=austin; Password=Tennis01"))
            {
                SecureString secureString = new SecureString();
                foreach (char entry in pass)
                {
                    secureString.AppendChar(entry);
                }
                secureString.MakeReadOnly();
                // SqlCredential cred = new SqlCredential("austin", secureString);

                try
                {
                    sqlCon.Open();
                }
                catch (SqlException exception)
                {
                    switch (exception.Number)
                    {
                        case 258:
                            Response.Redirect("TimeOutResponse.aspx");
                            break;
                        default:
                            throw;
                    }
                }
                string query = "SELECT COUNT(1) FROM Employees WHERE EmployeeID=@employeeID AND Employee_Password=@password";
                SqlCommand sqlCmd = new SqlCommand(query, sqlCon);
                sqlCmd.Parameters.AddWithValue("@employeeID", Convert.ToInt32(txtUsername.Text.Trim()));
                sqlCmd.Parameters.AddWithValue("@password", txtPassword.Text.Trim());
                int count = Convert.ToInt32(sqlCmd.ExecuteScalar());
                if (count == 1)
                {
                    Session["username"] = Convert.ToInt32(txtUsername.Text.Trim());
                    Response.Redirect("Default.aspx");
                } else
                {
                    lblErrorMessage.Visible = true;
                }

            }
        }
    }
}